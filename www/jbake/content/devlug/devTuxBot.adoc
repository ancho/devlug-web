= devTuxBot 
DebXWoody
2019-06-11
:jbake-type: post 
:jbake-status: published
:jbake-tags: devlug 
:jbake-updated: Di 11. Jun 11:33:42 CEST 2019
:idprefix:
:toc:
:sectanchors:
:sectlinks:

[abstract]
devTuxBot - Der IRC Bot

== IRC Bot
Unser IRC Bot devTuxBot ist ein link:http://www.eggheads.org/[eggdrop]. Der Bot
ist in C geschrieben und kann Tcl-Sckripte ausführen. Die Scripts für unseren
Bot liegen auf link:https://gitlab.com/devlug/devel/devTuxBot[gitlab].

== Befehle

----
!w <ort>     - Das Wetter für den aktuelle Ort
!task        - Aktuelle Liste der Tagesordnungspunkte
!r           - Die nächsten Termine für die devLUG
!go          - Beginn einer #devLUG
!topic       - Setzt das Thema des Channels 
----

== Timer
Der Bot hat 3 Timer. Er meldet sich jeden Tag um 6:00 Uhr, 18:00 Uhr und 20:30
Uhr. 

----
@devTuxBot | [06:00] Guten Morgen, devLUG! Es ist Tue Jun 11 06:00:00 2019! Wir wünschen euch einen schönen Tag.                                                    │
@devTuxBot | [06:00] Termine für Dienstag, den 11. Juni 2019 (heute):                                                                                               │
@devTuxBot | [06:00] #devLUG am Dienstag, den 25. Juni 2019                                                                                                         │
@devTuxBot | [06:00] #devLUG am Donnerstag, den 13. Juni 2019      
----

----
@devTuxBot | [20:30] Die nächste #devLUG ist am 2019-06-13 noch 3 Tage.
----

Hier ein kleines Beispiel. Es wird ein Timer um 6:00 morgens verwendet, die
Funktion say_goodmorning aufzurufen. Der Bot gibt dann die Informationen, wie
sie oben zu sehen sind, im IRC Channel aus.

----
bind time - "00 06 * * *" say_goodmorning	;# Timer um 06:00

proc say_goodmorning {a c d e f} {
	set current [ctime [unixtime]]
	putmsg #devlug "Guten Morgen, devLUG! Es ist $current! Wir wünschen euch einen schönen Tag."
	set option "/home/devlug/.reminders"
	set retval [catch { exec remind $option } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
        putmsg #devlug "$rec"
	}
}
----


