= Referenzkarte IRC
stop5
2018-08-02
:jbake-type: post 
:jbake-status: published
:jbake-tags: refcard, irc
:jbake-updated:  Do 2. Aug 19:17:59 CEST 2018
:jbake-autoren: stop5
:idprefix:
:toc:
[abstract]
Referenzkarte für IRC

== Kommandos

.Kommandos
|===
| Kommando | Beschreibung

|JOIN <channel> [<key>]
| Betritt einen Kanal. Wenn der Kanal einen key benötigt wird dieser hinter dem Kanal angegeben werden.

| AWAY [<reason>]
| zeigt anderen Nutzen an das  man nicht Aktiv mitliest.

| INVITE <user> <channel>
| Lädt den Benutzer in den Kanal ein.

| KICK <channel> <client> [<message>]
| Entfernt den Benutzer aus dem Kanal mit einer Optionalen Nachricht

| MODE [<user> [<flags>]]
| wenn flags nicht mitgegeben wird, werden ,wenn der User die Rechte hat, sie Flags auf dem entsprechenden User gesetzt, ansonsten werden sie abgefragt. Wenn user nicht mitgegeben wird, wird der eigene benutzt.

| NAMES [<channel>*]
| Fragt die nutzer in den Angegebenen Kanälen ab. Wenn einzelne Benutzer OP oder voice Status haben, werden ein @, rep. + vorangestellt, auf nicht IRCv3 Servern wird nur das Höhere Zeichen vorangestellt.

| TOPIC <channel> [<topic>]
| Wenn topic nicht mitgegeben wird, wird die TOPIc des Kanals abgefragt, ansonsten gesetzt(Entsprechende Rechte vorrausgesetzt)

|===

== Textmodifier

.Textmodifier
|===
| Modifier | Beschreibung

| 03~16~
| Setzt die Farbe zurück
| 03~16~XX
| Setzt die Textfarbe(XX). Die entsprechenden Codes stehen in der Tabelle Farben.
| 03~16~XX,YY
| Setzt die Text-(XX) und Hintergrundfarbe(YY). Die entsprechenden Codes stehen in der Tabelle <<Farben>>.

| 1D~16~
| Schreibt den Folgenden Text Kursiv

| 1F~16~
| Unterstreicht den folgenden Text

| 02~16~
| Macht den folgenden Text Fett

| 0F~16~
| setzt die Modifier zurück
|===

== Farben

.Farben
|===
| Farbcode | Farbe | Beispiel

| 00
| Weiß
| #FFFFFF image:img/refcards/irc/FFFFFF.png[]

| 01
| Schwarz
| #000000 image:img/refcards/irc/000000.png[]

| 02
| Blau
| #000080 image:img/refcards/irc/000080.png[]

| 03
| Grün
| #00FF00 image:img/refcards/irc/00FF00.png[]

| 04
| Rot
| #FF0000 image:img/refcards/irc/FF0000.png[]

| 05
| Braun
| #800000 image:img/refcards/irc/800000.png[]

| 06
| Violett
| #A146FF image:img/refcards/irc/A146FF.png[]

| 07
| Orange
| #724A25 image:img/refcards/irc/724A25.png[]

| 08
| Gelb
| #FFFF00 image:img/refcards/irc/FFFF00.png[]

| 09
| Hellgrün
| #BFFF00 image:img/refcards/irc/BFFF00.png[]

| 10
| Türkis
| #008080 image:img/refcards/irc/008080.png[]

| 11
| Cyan
| #00FFFF image:img/refcards/irc/00FFFF.png[]

| 12
| Hellblau
| #002366 image:img/refcards/irc/002366.png[]

| 13
| Fuchsia
| #CA1F7B image:img/refcards/irc/CA1F7B.png[]

| 14
| Grau
| #888888 image:img/refcards/irc/888888.png[]

| 15
| Hellgrau
| #DDDDDD image:img/refcards/irc/DDDDDD.png[]

| 99
| Normale Textfarbe

|===
