= Referenzkarte tmux
DebXWoody
2018-04-28
:jbake-type: post
:jbake-status: published
:jbake-tags: refcard, tmux
:idprefix:
:toc:
[abstract]
Referenzkarte für tmux.

== Starten und beenden

* tmux - tmux starten (neue Session)
* tmux a / tmux att / tmux attach - tmux starten und Session wieder herstellen
* STRG + b - D - tmux detach beendet tmux und legt die Session in den Hintergrund    

== Window (Fenster / Reiter)

.Window Keys
|===
|Key | Beschreibung 

|STRG + b - C
|Erstellt einen neuen Reiter (Tab)

|STRG + b - (0-9)
|Wechselt zum Window (Reiter) 0,1,2..9

|STRG + b - W
|Liste von Fenster anzeigen und auswählen

|STRG + b - ,
|Fenster umbenennen

|STRG + b - f
|Textsuche im aktuellen Fenster
|===

== Pane (Fensterscheibe)

.Pane Keys
|===
|Key | Beschreibung 

|STRG + b - %
|Fenster in zwei Scheiben aufteilen (rechts und link)

|STRG + b - "
|Fenster in zwei Scheiben aufteilen (oben und unten)

|STRG + b - q
|Index der panes anzeigen

|STRG + b - o
|Springe zum nächsten pane

|STRG + b - (UP, DOWN, LEFT oder RIGHT)
|Wechsel zum pane

|STRG + b - SPACE
|Nächstes pane layout

|STRG + b - ALT + (1 - 5)
|Pane layout auswählen 1 - 5

|STRG + b - ALT +  (UP, DOWN, LEFT oder RIGHT)
|Anpassen der Größe für die pane.

|STRG + b - } 
|Tauschen der panes (aktuellen mit nächstem)
|===

== Session

.Session Keys
|===
|Key | Beschreibung 

|STRG + b - ) 
|Spring zur nächsten Session
|===

== Sonstiges

.Sonstiges  Keys
|===
|Key | Beschreibung 

|STRG + b - t 
|Uhrzeit anzeigen

|STRG + b - $
|Session umbenennen
|===


