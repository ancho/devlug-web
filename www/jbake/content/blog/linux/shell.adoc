= Linux Shell 
DebXWoody
2019-04-07
:jbake-type: post
:jbake-status: published
:jbake-updated: 2019-04-07
:jbake-regal: Linux
:idprefix:
:toc:
[abstract]
Informationen über Linux Shell.

== Allgemeine Informationen
Allgemeine Informationen auf link:https://de.wikipedia.org/wiki/Unix-Shell[Wikipedia].

== Bash

* link:https://www.gnu.org/software/bash/manual/[Dokumentation]

== Tools

* link:https://github.com/tmux/tmux/wiki[tmux]
* link:https://github.com/powerline/powerline[powerline]


== Links
* Advanced Bash-Scripting Guide: http://tldp.org/LDP/abs/html/
* The Bash Guide: https://guide.bash.academy/
* Shell Scripting Tutorial: https://www.shellscript.sh/
