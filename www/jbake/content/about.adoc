= Über uns
DebXWoody <debxwoody@devlug.de>
2019-03-31
:jbake-type: page
:jbake-tags: devLUG
:jbake-status: published
== Die devLUG 
Die **devLUG** ist eine Gruppe von Leuten die sich für GNU/Linux und FLOSS
interessieren. Es handelt sich um eine virtuelle Linux User Group,
welche ausschließlich Dienste im Internet für die Kommunikation nutzt.

=== E-Mail
Wir sind per E-Mail unter  kontakt@devlug.de erreichbar. Die E-Mails
werden an _ancho_ und _DebXWoody_ weitergeleitet.

== Mailingliste
Unsere Mailingliste auf Framasoft. https://framalistes.org/sympa/subscribe/devlug

[#devlug]
=== #devlug
Wie die meisten uns bekannten LUGs haben auch wir einen 'Stammtisch'. Dieser ist jedoch nicht in einem Lokal sondern wo
auch immer du möchtest. Beispielsweise in deinem Garten, im Urlaub,... Unser Stammtisch findet im Internet Relay Chat (IRC) auf
freenode.net statt. Du brauchst nur eine Internet Verbindung.

Unsere #devlug ist alle 2 Wochen Dienstags und Donnerstags im Wechsel.
Treffen ist ab 20:30 Uhr im IRC Freenode - #devlug.

Webchat auf Freenode.net: http://webchat.freenode.net/?channels=%23devlug

Termine sind sind als ics Datei verfügbar: https://www.devlug.de/stammtisch.ics

[#XMPP]
=== XMPP MUC
Wir nutzen einen XMPP Multi-User Chat. Der Raum ist auf einem Server
der Anoxinon e.V.
`devlug@conference.anoxinon.me`

=== Mumble
Die devLUG nutzt Mumble als Voice over IP Dienst. 
Wir haben einen Raum auf den Servern von Anoxinon e.V.
`mumble.anoxinon.de`

[#git]
=== devLUG auf GitLab
Du findest uns auf GitLab unter: https://gitlab.com/devlug.
Auf dieser Plattform arbeiten wir über Git-Repositories zusammen. Unsere Projekte findest du in dieser Gruppe. So
ist beispielsweise unsere Homepage auf https://gitlab.com/devlug/devlug-web. 

=== Federated network 
Die devLUG im federated network: https://libranet.de/profile/devlug/

== GnuPG
Der devLUG OpenPGP Key

----
pub   rsa4096/0xE9F8EA9CB497F86E 2018-11-03 [SC] [verfällt: 2021-11-02]
  Schl.-Fingerabdruck = 3387 774D 6224 E96A 6478  656B E9F8 EA9C B497 F86E
uid                [vollständig] devLUG <devlug@devlug.de>
----

== Policy Manual
Die wichtigsten Informationen sind im Policy Manual 
link:https://www.devlug.de/devlug/devLUG-Policy-Manual.html[Dokument] beschrieben.

